//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

namespace nya_system
{

enum button_code
{
    //X11 codes

    key_return    =0xff0d,
    key_escape    =0xff1b,
    key_space     =0x0020,
    key_tab       =0xff09,
    key_pause     =0xff13,

    key_shift     =0xffe1,
    key_shift_r   =0xffe2,
    key_control   =0xffe3,
    key_control_r =0xffe4,
    key_alt       =0xffe9,
    key_alt_r     =0xffea,
    key_capital   =0xffe5,

    key_up        =0xff52,
    key_down      =0xff54,
    key_left      =0xff51,
    key_right     =0xff53,

    key_end       =0xff57,
    key_home      =0xff50,
    key_insert    =0xff63,
    key_delete    =0xffff,
    key_backspace =0xff08,

    key_a         =0x0061,
    key_0         =0x00300,
    key_1         =0x0031,
    key_f1        =0xffbe,

    //additional codes

    key_back      =0xffff01,
};

}
