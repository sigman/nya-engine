#nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

import bpy
from nya.nya_nms import nms_mesh

def export_nms(context, props, filepath):
    mesh = nms_mesh()

    materials = []

    for obj in context.scene.objects:
        if obj.type == 'MESH':
            me = obj.data

            group = nms_mesh.nms_group()

            mats = me.materials[:]
            if not mats:
                print("No materials found in mesh", me.name)
                continue

            if len(mats) > 1:
                print("More than 1 material per mesh is not supported", me.name)
                return

            group.mat_idx = mesh.add_material(mats[0].name)
            if group.mat_idx >= len(materials):
                materials.append(mats[0])

            if not me.tessfaces and me.polygons:
                me.calc_tessface()

            group.name = obj.name
            group.offset = mesh.vcount

            matrix = obj.matrix_world.copy()
            for face in me.tessfaces:
                vpos = []
                for verti in face.vertices:
                    vpos.append((matrix * me.vertices[verti].co)[:])

                #triangulate quad
                if len(vpos) == 4:
                    vpos.append(vpos[-4]) #0
                    vpos.append(vpos[-3]) #2

                #ToDo: tcs, normal, bones

                for v in vpos:
                    mesh.verts_data.append(v[0])
                    mesh.verts_data.append(v[1])
                    mesh.verts_data.append(v[2])

                mesh.vcount += len(vpos)

            group.count = mesh.vcount - group.offset
            mesh.groups.append(group)

    a = nms_mesh.nms_vertex_attribute()
    a.type = 0
    a.dimension = 3
    a.semantics = "pos"
    mesh.vert_attr.append(a);

    #ToDo: tcs, normal, bones

    for i,m in enumerate(mesh.materials):
        m.params.append(nms_mesh.nms_param("nya_material", "default.txt"))
        for mtex in materials[i].texture_slots:
            if not mtex or not mtex.texture or mtex.texture.type != 'IMAGE':
                continue
            image = mtex.texture.image
            if not image:
                continue
            filename = bpy.path.basename(image.filepath)
            if mtex.use_map_color_diffuse:
                m.textures.append(nms_mesh.nms_param("diffuse", filename))
            if mtex.use_map_normal:
                m.textures.append(nms_mesh.nms_param("bump", filename))
            if mtex.use_map_ambient:
                m.textures.append(nms_mesh.nms_param("ambient", filename))
            if mtex.use_map_color_spec:
                m.textures.append(nms_mesh.nms_param("specular", filename))

    #ToDo: skeleton

    mesh.create_indices()
    mesh.write(filepath)
