//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#pragma once

#include "log.h"

namespace nya_log
{

class plain_file_log: public log_base
{
public:
    bool open(const char*file_name);
    void close();

    plain_file_log(const char *file_name=""): m_file_name(file_name?file_name:"") {}

private:
    virtual void output(const char *string);

private:
    std::string m_file_name;
};

}
