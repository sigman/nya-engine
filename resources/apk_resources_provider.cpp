//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "apk_resources_provider.h"

#ifdef __ANDROID__
    #include <jni.h>
    #include <android/asset_manager.h>
#endif

namespace nya_resources
{

namespace { AAssetManager *asset_mgr=0; }

void apk_resources_provider::set_asset_manager(AAssetManager *mgr) { asset_mgr=mgr; }

bool apk_resources_provider::set_folder(const char *folder)
{
    if(!asset_mgr || !folder)
    {
        m_folder.clear();
        return false;
    }

    m_folder.assign(folder);

#ifdef __ANDROID__
    AAssetDir *asset_dir=AAssetManager_openDir(asset_mgr,folder);
    if(!asset_dir)
        return false;

    AAssetDir_close(asset_dir);
#endif

    if(!m_entries.empty())
        update_entries();

    return true;
}

bool apk_resources_provider::has(const char *name)
{
    if(!name)
        return false;

    if(m_entries.empty())
        update_entries();

    for(int i=0;i<(int)m_entries.size();++i)
    {
        if(m_entries[i]==name)
            return true;
    }

    return false;
}

void apk_resources_provider::update_entries()
{
    m_entries.clear();
    if(!asset_mgr)
        return;

#ifdef __ANDROID__
    AAssetDir *asset_dir=AAssetManager_openDir(asset_mgr,m_folder.c_str());
    if(!asset_dir)
        return;

    const char *filename = 0;
    while((filename=AAssetDir_getNextFileName(asset_dir)))
        m_entries.push_back(filename);

    AAssetDir_close(asset_dir);
#endif
}

int apk_resources_provider::get_resources_count()
{
    if(m_entries.empty())
        update_entries();

    return (int)m_entries.size();
}

const char *apk_resources_provider::get_resource_name(int idx)
{
    if(m_entries.empty())
        update_entries();

    if(idx<0 || idx>=(int)m_entries.size())
        return 0;

    return m_entries[idx].c_str();
}

#ifdef __ANDROID__
namespace
{

class apk_resource: public resource_data
{
public:
    apk_resource(AAsset *asset): m_asset(asset) { m_size=AAsset_getLength(m_asset); }

    void release() { AAsset_close(m_asset); delete this; }

    size_t get_size() { return m_size; }

    bool read_all(void *data)
    {
        if(!data)
            return false;

        AAsset_seek(m_asset,0,SEEK_SET);
        return AAsset_read(m_asset,data,m_size)==m_size;
    }

    bool read_chunk(void *data,size_t size,size_t offset)
    {
        if(!data)
            return false;

        const off_t pos=AAsset_seek(m_asset,offset,SEEK_SET);
        if(pos== -1 || pos!=offset)
            return false;

        return AAsset_read(m_asset,data,size)==size;
    }

private:
    AAsset *m_asset;
    size_t m_size;
};

}
#endif

resource_data *apk_resources_provider::access(const char *name)
{
    if(!asset_mgr || !name)
        return 0;

    log()<<"apk_resources_provider access"<<name<<"\n";

#ifdef __ANDROID__
    AAsset *asset=AAssetManager_open(asset_mgr,name,AASSET_MODE_UNKNOWN);
    if(!asset)
        return 0;

    return new apk_resource(asset);
#else
    return 0;
#endif
}

}
