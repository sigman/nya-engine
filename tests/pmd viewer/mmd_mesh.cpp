//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "mmd_mesh.h"

bool mmd_mesh::load(const char *name)
{
    unload();

    nya_scene::mesh::register_load_function(pmx_loader::load,false);
    nya_scene::mesh::register_load_function(pmd_loader::load,false);

    return mesh::load(name) && init_vbo();
}

bool mmd_mesh::init_vbo()
{
    m_pmd_data=pmd_loader::get_additional_data(*this);
    m_pmx_data=pmx_loader::get_additional_data(*this);

    if(!is_mmd())
        return true;

    const nya_render::vbo &vbo=internal().get_shared_data()->vbo;
    nya_memory::tmp_buffer_ref buf;
    if(!vbo.get_vertex_data(buf))
        return false;

    const int fstride=vbo.get_vert_stride()/4;
    m_original_vertex_data.resize(fstride*vbo.get_verts_count());
    buf.copy_to(&m_original_vertex_data[0],m_original_vertex_data.size()*4);
    buf.free();

    m_vertex_data.resize(vbo.get_verts_count());
    const char *tcdata=(char *)&m_original_vertex_data[0]+vbo.get_tc_offset(0);
    for(int i=0;i<vbo.get_verts_count();++i)
        memcpy(&m_vertex_data[i].tc,tcdata+vbo.get_vert_stride()*i,sizeof(nya_math::vec2));

    if(m_pmd_data)
        m_morphs.resize(m_pmd_data->morphs.size());
    else if(m_pmx_data)
        m_morphs.resize(m_pmx_data->morphs.size());

    m_sk_pos_buf.resize(get_bones_count());

    m_vbo.set_vertices(0,3);
    m_vbo.set_normals(3*4);
    m_vbo.set_tc(0,(3+3)*4,2);
    update(0);

    return true;
}

void mmd_mesh::unload()
{
    mesh::unload();
    m_vertex_data.clear();
    m_original_vertex_data.clear();
    m_pmd_data=0;
    m_pmx_data=0;
    m_vbo.release();
    m_morphs.clear();
}

void mmd_mesh::set_anim(const nya_scene::animation &anim,int layer)
{
    mesh::set_anim(anim,layer);
    int idx=-1;
    for(int i=0;i<int(m_anims.size());++i)
    {
        if(m_anims[i].layer==layer)
        {
            idx=i;
            break;
        }
    }

    if(idx<0)
    {
        idx=int(m_anims.size());
        m_anims.resize(idx+1);
    }

    m_anims[idx].curves_map.clear();
    m_anims[idx].curves_map.resize(m_morphs.size());
    if(!anim.get_shared_data().is_valid())
        return;

    if(m_pmd_data)
    {
        for(int i=0;i<int(m_morphs.size());++i)
            m_anims[idx].curves_map[i]=anim.get_shared_data()->anim.get_curve_idx(m_pmd_data->morphs[i].name.c_str());
    }
    else if(m_pmx_data)
    {
        for(int i=0;i<int(m_morphs.size());++i)
            m_anims[idx].curves_map[i]=anim.get_shared_data()->anim.get_curve_idx(m_pmx_data->morphs[i].name.c_str());
    }
}

void mmd_mesh::update(unsigned int dt)
{
    mesh::update(dt);

    if(!is_mmd())
        return;

    for(int i=0;i<m_anims.size();++i)
    {
        const nya_scene::animation_proxy &ap=get_anim(m_anims[i].layer);
        if(ap.is_valid())
        {
            if(ap->get_shared_data().is_valid())
            {
                const nya_render::animation &a=ap->get_shared_data()->anim;
                if(i==0)
                {
                    for(int j=0;j<m_anims[i].curves_map.size();++j)
                    {
                        if(m_morphs[j].overriden)
                            continue;

                        m_morphs[j].value=a.get_curve(m_anims[i].curves_map[j],get_anim_time(m_anims[i].layer))*ap->get_weight();
                    }
                }
                else
                {
                    for(int j=0;j<m_anims[i].curves_map.size();++j)
                    {
                        if(m_morphs[j].overriden)
                            continue;

                        m_morphs[j].value+=a.get_curve(m_anims[i].curves_map[j],get_anim_time(m_anims[i].layer))*ap->get_weight();
                    }
                }
            }
        }
    }

    //update group morphs first

    if(m_pmx_data)
    {
        for(int i=0;i<(int)m_pmx_data->morphs.size();++i)
        {
            const pmx_loader::additional_data::morph &m=m_pmx_data->morphs[i];
            if(m.type!=pmx_loader::morph_type_group)
                continue;

            const float delta=m_morphs[i].value-m_morphs[i].last_value;
            if(fabsf(delta)<0.01f)
                continue;

            const pmx_loader::additional_data::group_morph &gm=m_pmx_data->group_morphs[m.idx];
            for(int j=0;j<(int)gm.morphs.size();++j)
                m_morphs[gm.morphs[j].first].value=m_morphs[i].value*gm.morphs[j].second;

            m_morphs[i].last_value=m_morphs[i].value;
        }
    }

    //ToDo: gpu skining and morphs

    const nya_render::skeleton &sk=get_skeleton();
    const nya_math::vec3 *sk_pos=(nya_math::vec3 *)sk.get_pos_buffer();
    const nya_math::quat *sk_rot=(nya_math::quat *)sk.get_rot_buffer();
    for(int i=0;i<sk.get_bones_count();++i)
        m_sk_pos_buf[i]=sk_pos[i]+sk_rot[i].rotate(-sk.get_bone_original_pos(i));
    sk_pos=&m_sk_pos_buf[0];

    if(m_pmx_data)
    {
        const pmx_loader::vert *verts=(const pmx_loader::vert *)&m_original_vertex_data[0];
        for(int i=0;i<(int)m_vertex_data.size();++i)
        {
            m_vertex_data[i].pos=verts[i].pos;
            m_vertex_data[i].normal=verts[i].normal;
        }
    }
    else
    {
        const pmd_loader::vert *verts=(const pmd_loader::vert *)&m_original_vertex_data[0];
        for(int i=0;i<(int)m_vertex_data.size();++i)
        {
            m_vertex_data[i].pos=verts[i].pos;
            m_vertex_data[i].normal=verts[i].normal;
        }
    }

    for(int i=0;i<m_morphs.size();++i)
    {
        if(m_morphs[i].value<=0.0f && m_morphs[i].last_value<=0.0f)
            continue;

        if(m_pmd_data)
        {
            const pmd_morph_data::morph &m=m_pmd_data->morphs[i];
            for(int j=0;j<int(m.verts.size());++j)
                m_vertex_data[m.verts[j].idx].pos+=m.verts[j].pos*m_morphs[i].value;
        }
        else if(m_pmx_data)
        {
            const pmx_loader::additional_data::morph &m=m_pmx_data->morphs[i];
            switch(m.type)
            {
                case pmx_loader::morph_type_vertex:
                {
                    const pmx_loader::additional_data::vert_morph &vm=m_pmx_data->vert_morphs[m.idx];
                    for(int j=0;j<int(vm.verts.size());++j)
                        m_vertex_data[vm.verts[j].idx].pos+=vm.verts[j].pos*m_morphs[i].value;
                }
                break;

                case pmx_loader::morph_type_uv:
                {
                    const pmx_loader::additional_data::uv_morph &vm=m_pmx_data->uv_morphs[m.idx];
                    const pmx_loader::vert *verts=(const pmx_loader::vert *)&m_original_vertex_data[0];
                    for(int j=0;j<int(vm.verts.size());++j)
                    {
                        const int idx=vm.verts[j].idx;
                        m_vertex_data[idx].tc=verts[idx].tc+vm.verts[j].tc*m_morphs[i].value;
                    }
                }
                break;

                case pmx_loader::morph_type_material:
                {
                    const float value=m_morphs[i].value;
                    const pmx_loader::additional_data::mat_morph &mm=m_pmx_data->mat_morphs[m.idx];
                    for(int j=0;j<int(mm.mats.size());++j)
                    {
                        const pmx_loader::additional_data::morph_mat &m=mm.mats[j];
                        const pmx_loader::additional_data::mat &om=m_pmx_data->materials[m.mat_idx];
                        nya_scene::material &mat=modify_material(m.mat_idx);

                        pmx_loader::pmx_material_params params;
                        pmx_loader::pmx_edge_params edge;

                        if(m.op==pmx_loader::additional_data::morph_mat::op_add)
                        {
                            params.ambient=om.params.ambient+m.params.ambient*value;
                            params.diffuse=om.params.diffuse+m.params.diffuse*value;
                            params.specular=om.params.specular+m.params.specular*value;
                            params.shininess=om.params.shininess+m.params.shininess*value;

                            edge.color=om.edge_params.color+m.edge_params.color*value;
                            edge.width=om.edge_params.width+m.edge_params.width*value;
                        }
                        else if(m.op==pmx_loader::additional_data::morph_mat::op_mult)
                        {
                            params.ambient=nya_math::vec3::lerp(om.params.ambient,om.params.ambient*m.params.ambient,value);
                            params.diffuse=nya_math::vec4::lerp(om.params.diffuse,om.params.diffuse*m.params.diffuse,value);
                            params.specular=nya_math::vec3::lerp(om.params.specular,om.params.specular*m.params.specular,value);
                            params.shininess=nya_math::lerp(om.params.shininess,om.params.shininess*m.params.shininess,value);

                            edge.color=nya_math::vec4::lerp(om.edge_params.color,om.edge_params.color*m.edge_params.color,value);
                            edge.width=nya_math::lerp(om.edge_params.width,om.edge_params.width*m.edge_params.width,value);
                        }

                        mat.set_param(mat.get_param_idx("amb k"),params.ambient,1.0f);
                        mat.set_param(mat.get_param_idx("diff k"),params.diffuse);
                        mat.set_param(mat.get_param_idx("spec k"),params.specular,params.shininess);

                        if(om.edge_group_idx>=0)
                        {
                            nya_scene::material &mat=modify_material(om.edge_group_idx);
                            mat.set_param(mat.get_param_idx("edge offset"),edge.width*0.02f,edge.width*0.02f,edge.width*0.02f,0.0f);
                            mat.set_param(mat.get_param_idx("edge color"),edge.color);
                        }
                    }
                }
                break;

                case pmx_loader::morph_type_bone:
                {
                    const pmx_loader::additional_data::bone_morph &bm=m_pmx_data->bone_morphs[m.idx];
                    for(int j=0;j<int(bm.bones.size());++j)
                    {
                        set_bone_pos(bm.bones[j].idx,bm.bones[j].pos*m_morphs[i].value,true);
                        nya_math::quat rot=bm.bones[j].rot;
                        rot.apply_weight(m_morphs[i].value);
                        set_bone_rot(bm.bones[j].idx,rot,true);
                    }
                }

                default: continue;
            }
        }

        m_morphs[i].last_value=m_morphs[i].value;
    }

    if(m_pmx_data)
    {
        const pmx_loader::vert *verts=(const pmx_loader::vert *)&m_original_vertex_data[0];
        for(int i=0;i<(int)m_vertex_data.size();++i)
        {
            nya_math::vec3 pos=m_vertex_data[i].pos,normal=m_vertex_data[i].normal;
            const int idx=verts[i].bone_idx[0];
            m_vertex_data[i].pos=(sk_pos[idx]+sk_rot[idx].rotate(pos))*verts[i].bone_weight[0],
            m_vertex_data[i].normal=sk_rot[idx].rotate(normal)*verts[i].bone_weight[0];
            for(int j=1;j<4;++j)
            {
                if(verts[i].bone_weight[j]<0.001f)
                    continue;

                const int idx=verts[i].bone_idx[j];
                m_vertex_data[i].pos+=(sk_pos[idx]+sk_rot[idx].rotate(pos))*verts[i].bone_weight[j],
                m_vertex_data[i].normal+=sk_rot[idx].rotate(normal)*verts[i].bone_weight[j];
            }
        }
    }
    else
    {
        const pmd_loader::vert *verts=(const pmd_loader::vert *)&m_original_vertex_data[0];
        for(int i=0;i<(int)m_vertex_data.size();++i)
        {
            nya_math::vec3 pos=m_vertex_data[i].pos,normal=m_vertex_data[i].normal;
            const int idx0=verts[i].bone_idx[0],idx1=verts[i].bone_idx[1];

            m_vertex_data[i].pos=nya_math::vec3::lerp(sk_pos[idx1]+sk_rot[idx1].rotate(pos),
                                                      sk_pos[idx0]+sk_rot[idx0].rotate(pos),verts[i].bone_weight);

            m_vertex_data[i].normal=nya_math::vec3::lerp(sk_rot[idx1].rotate(normal),
                                                         sk_rot[idx0].rotate(normal),verts[i].bone_weight);
        }
    }

    m_vbo.set_vertex_data(&m_vertex_data[0],(int)sizeof(vert),(int)m_vertex_data.size());
}

void mmd_mesh::draw_group(int group_idx,const char *pass_name) const
{
    if(!is_mmd())
    {
        mesh::draw_group(group_idx,pass_name);
        return;
    }

    if(!internal().get_shared_data().is_valid())
        return;

    if(group_idx<0 || group_idx>=get_groups_count())
        return;

    /*
     if(m_mesh.internal().m_has_aabb) //ToDo
     {
     if(get_camera().is_valid() && !get_camera()->get_frustum().test_intersect(get_aabb()))
     return;
     }
     */
    nya_scene::transform::set(internal().get_transform());
    nya_scene::shader_internal::set_skeleton(&get_skeleton());

    const nya_scene::shared_mesh &sh=*internal().get_shared_data().operator ->();
    sh.vbo.bind_indices();
    m_vbo.bind_verts();

    const nya_scene::material &m=get_material(group_idx);
    m.internal().set(pass_name);
    m_vbo.draw(sh.groups[group_idx].offset,sh.groups[group_idx].count);
    m.internal().unset();
    m_vbo.unbind();
    sh.vbo.unbind();
    nya_scene::shader_internal::set_skeleton(0);
}

void mmd_mesh::draw(const char *pass_name) const
{
    if(!is_mmd())
    {
        mesh::draw(pass_name);
        return;
    }

    for(int i=0;i<get_groups_count();++i)
        draw_group(i);
}

const char *mmd_mesh::get_morph_name(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return 0;

    if (m_pmd_data)
        return m_pmd_data->morphs[idx].name.c_str();

    if (m_pmx_data)
        return m_pmx_data->morphs[idx].name.c_str();

    return 0;
}

pmd_morph_data::morph_kind mmd_mesh::get_morph_kind(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return pmd_morph_data::morph_base;

    if (m_pmd_data)
        return m_pmd_data->morphs[idx].kind;

    if (m_pmx_data)
        return m_pmx_data->morphs[idx].kind;

    return pmd_morph_data::morph_base;
}

float mmd_mesh::get_morph(int idx) const
{
    if(idx<0 || idx>=get_morphs_count())
        return 0.0f;

    return m_morphs[idx].value;
}
